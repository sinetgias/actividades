"""actividades URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import name
from telnetlib import LOGOUT
from django import views
from django.conf.urls import url,include
from django.contrib import admin
from django.urls import path
from apps import usuarios
from apps.servidores.views import SistemasView
from apps.actividades.views import datatable_actividadesView, lista_empleado, lista_sistemas, ListaActividad,Create_actividad,lista_actividad_API,actividades_generalesView, Actividades_List_API, Update_actividad, Eliminar_Actividad
from apps.usuarios.views import login, logout, index

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^listar/$', ListaActividad.as_view(), name='actividades_index') ,
    path('add/',Create_actividad.as_view(), name = 'add'),
    path('ajax_sistemas/',SistemasView.as_view(), name = 'sistemas'),
    path('ajax_Users/',lista_empleado.as_view(), name= 'solicitas'),
    path('ajax_dns/',lista_sistemas.as_view(), name= 'lista_sistemas'),
    path('ajax_actividades/',lista_actividad_API.as_view(), name= 'lista_actividades_ajax'),  # Verificar esta json para el datatable 
    url('^my/datatable/data/$', datatable_actividadesView.as_view(), name='order_list_json'),   # Verificar esta API Verificar esta API para el datatable
    path('listar_actividades/',actividades_generalesView.as_view(),name='actividades_generales'),
    #Usuarios login
    path('',index.as_view(), name='inicio'),
    # path('home',bienvenido),
    path('login', login),
    path('logout',logout, name='logout'),
    #Lista de Actividades.
    url(r'^lista_actividades_genericas/$',Actividades_List_API.as_view(),name='actividades_genericas'), #Lista basada en clases Genericas.
    url(r'^delete-actividad/(?P<pk>\d+)/$', Eliminar_Actividad.as_view(), name='delete-actividad'),
    url(r'^editar-actividad/(?P<pk>\d+)/$', Update_actividad.as_view(), name='editar-actividad'), #Ejemplo Jonathan
]
