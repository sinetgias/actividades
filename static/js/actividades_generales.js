
let table

table = $('#demo').DataTable(
    {
        // "language": {"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},
            language: {
                
                    "aria": {
                        "sortAscending": "Activar para ordenar la columna de manera ascendente",
                        "sortDescending": "Activar para ordenar la columna de manera descendente"
                    },
                    "autoFill": {
                        "cancel": "Cancelar",
                        "fill": "Rellene todas las celdas con <i>%d<\/i>",
                        "fillHorizontal": "Rellenar celdas horizontalmente",
                        "fillVertical": "Rellenar celdas verticalmente"
                    },
                    "buttons": {
                        "collection": "Colección",
                        "colvis": "Visibilidad",
                        "colvisRestore": "Restaurar visibilidad",
                        "copy": "Copiar",
                        "copyKeys": "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
                        "copySuccess": {
                            "1": "Copiada 1 fila al portapapeles",
                            "_": "Copiadas %d fila al portapapeles"
                        },
                        "copyTitle": "Copiar al portapapeles",
                        "csv": "CSV",
                        "excel": "Excel",
                        "pageLength": {
                            "-1": "Mostrar todas las filas",
                            "_": "Mostrar %d filas"
                        },
                        "pdf": "PDF",
                        "print": "Imprimir",
                        "createState": "Crear Estado",
                        "removeAllStates": "Borrar Todos los Estados",
                        "removeState": "Borrar Estado",
                        "renameState": "Renombrar Estado",
                        "savedStates": "Guardar Estado",
                        "stateRestore": "Restaurar Estado",
                        "updateState": "Actualizar Estado"
                    },
                    "infoThousands": ",",
                    "loadingRecords": "Cargando...",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchBuilder": {
                        "add": "Añadir condición",
                        "button": {
                            "0": "Constructor de búsqueda",
                            "_": "Constructor de búsqueda (%d)"
                        },
                        "clearAll": "Borrar todo",
                        "condition": "Condición",
                        "deleteTitle": "Eliminar regla de filtrado",
                        "leftTitle": "Criterios anulados",
                        "logicAnd": "Y",
                        "logicOr": "O",
                        "rightTitle": "Criterios de sangría",
                        "title": {
                            "0": "Constructor de búsqueda",
                            "_": "Constructor de búsqueda (%d)"
                        },
                        "value": "Valor",
                        "conditions": {
                            "date": {
                                "after": "Después",
                                "before": "Antes",
                                "between": "Entre",
                                "empty": "Vacío",
                                "equals": "Igual a",
                                "not": "Diferente de",
                                "notBetween": "No entre",
                                "notEmpty": "No vacío"
                            },
                            "number": {
                                "between": "Entre",
                                "empty": "Vacío",
                                "equals": "Igual a",
                                "gt": "Mayor a",
                                "gte": "Mayor o igual a",
                                "lt": "Menor que",
                                "lte": "Menor o igual a",
                                "not": "Diferente de",
                                "notBetween": "No entre",
                                "notEmpty": "No vacío"
                            },
                            "string": {
                                "contains": "Contiene",
                                "empty": "Vacío",
                                "endsWith": "Termina con",
                                "equals": "Igual a",
                                "not": "Diferente de",
                                "startsWith": "Inicia con",
                                "notEmpty": "No vacío",
                                "notContains": "No Contiene",
                                "notEnds": "No Termina",
                                "notStarts": "No Comienza"
                            },
                            "array": {
                                "equals": "Igual a",
                                "empty": "Vacío",
                                "contains": "Contiene",
                                "not": "Diferente",
                                "notEmpty": "No vacío",
                                "without": "Sin"
                            }
                        },
                        "data": "Datos"
                    },
                    "searchPanes": {
                        "clearMessage": "Borrar todo",
                        "collapse": {
                            "0": "Paneles de búsqueda",
                            "_": "Paneles de búsqueda (%d)"
                        },
                        "count": "{total}",
                        "emptyPanes": "Sin paneles de búsqueda",
                        "loadMessage": "Cargando paneles de búsqueda",
                        "title": "Filtros Activos - %d",
                        "countFiltered": "{shown} ({total})",
                        "collapseMessage": "Colapsar",
                        "showMessage": "Mostrar Todo"
                    },
                    "select": {
                        "cells": {
                            "1": "1 celda seleccionada",
                            "_": "%d celdas seleccionadas"
                        },
                        "columns": {
                            "1": "1 columna seleccionada",
                            "_": "%d columnas seleccionadas"
                        }
                    },
                    "thousands": ",",
                    "datetime": {
                        "previous": "Anterior",
                        "hours": "Horas",
                        "minutes": "Minutos",
                        "seconds": "Segundos",
                        "unknown": "-",
                        "amPm": [
                            "am",
                            "pm"
                        ],
                        "next": "Siguiente",
                        "months": {
                            "0": "Enero",
                            "1": "Febrero",
                            "10": "Noviembre",
                            "11": "Diciembre",
                            "2": "Marzo",
                            "3": "Abril",
                            "4": "Mayo",
                            "5": "Junio",
                            "6": "Julio",
                            "7": "Agosto",
                            "8": "Septiembre",
                            "9": "Octubre"
                        },
                        "weekdays": [
                            "Domingo",
                            "Lunes",
                            "Martes",
                            "Miércoles",
                            "Jueves",
                            "Viernes",
                            "Sábado"
                        ]
                    },
                    "editor": {
                        "close": "Cerrar",
                        "create": {
                            "button": "Nuevo",
                            "title": "Crear Nuevo Registro",
                            "submit": "Crear"
                        },
                        "edit": {
                            "button": "Editar",
                            "title": "Editar Registro",
                            "submit": "Actualizar"
                        },
                        "remove": {
                            "button": "Eliminar",
                            "title": "Eliminar Registro",
                            "submit": "Eliminar",
                            "confirm": {
                                "_": "¿Está seguro que desea eliminar %d filas?",
                                "1": "¿Está seguro que desea eliminar 1 fila?"
                            }
                        },
                        "multi": {
                            "title": "Múltiples Valores",
                            "restore": "Deshacer Cambios",
                            "noMulti": "Este registro puede ser editado individualmente, pero no como parte de un grupo.",
                            "info": "Los elementos seleccionados contienen diferentes valores para este registro. Para editar y establecer todos los elementos de este registro con el mismo valor, haga click o toque aquí, de lo contrario conservarán sus valores individuales."
                        },
                        "error": {
                            "system": "Ha ocurrido un error en el sistema (<a target=\"\\\" rel=\"\\ nofollow\" href=\"\\\"> Más información<\/a>)."
                        }
                    },
                    "decimal": ".",
                    "emptyTable": "No hay datos disponibles en la tabla",
                    "zeroRecords": "No se encontraron coincidencias",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                    "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total de entradas)",
                    "lengthMenu": "Mostrar _MENU_ entradas",
                    "stateRestore": {
                        "removeTitle": "Eliminar",
                        "creationModal": {
                            "search": "Buscar"
                        }
                    }
                
            
        },
        "processing": true,
        "serverSide":true,
        "ajax": 'http://10.2.8.116/my/datatable/data/',
        "type":"GET",
        "order": [[ 0, 'desc' ]],
        "columnDefs": [{targets: [0], visible: true}, //Se coloca en false y la posicion empezando de 0 para no visualizar en la tabla esa columna
        
        // {
        //     targets: 0,
        //     "width": "0px"
        // },
        {
            targets: 1,
            "width": "300px"
        },
        {
            targets: 2,
            "width": "100px"
        },
        {
            targets: 3,
            "width": "120px"
        },
        {
            targets: 4,
            "width": "150px"
        },
        {
            targets: 5,
            "width": "100px"
        },
        
        {
            targets: 7,
            "width": "150px"
        },  
        {
            targets: 8,
            "orderable": false,  //Se puede desactivar el que sea ordenable la columna como en el HTML
            "width": "150px",
            render: function (data, type, full) {
                var opciones = $('<span>');
                // var detalle = $('<a href="/detalle_cliente/' + full[0] + '"title="Detalle registro"><button class="btn">   detalle</button></a> ')
                var editar = $('<a href="/editar-actividad/' + full[0] + '/" title="Editar registro"> <button class="btn btn-primary"><i class="icon wb-edit""></i></button></a>');
                var eliminar = $('<a href="/delete-actividad/' + full[0] + '/" title="Eliminar registro" class="btn-sm"><button class="btn btn-warning"><i class="icon wb-trash" aria-required="true"></i></button></a>');
                // var editar = $('<a href="/editar-prestamo/' + full[0] + '/" title="Editar registro"> <button class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"><i class="icon wb-edit""></i></button></a>');
                // var eliminar = $('<a href="/delete-prestamo/' + full[0] + '/" title="Eliminar registro" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"><i class="icon wb-trash" aria-required="true"></i></button></a>');
                opciones.append(editar,eliminar);
                return opciones.html();
            }
        }],     
     });

let id=0;
$('#guardar-modal').on('click', function (e) {    
    //alert('')
    if ($('#tags').val() == []) {
        $('#tags').val(['']);
        //alert('Debe Seleccionar un Sistema');
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger"><a href="" class="close" data-dismiss="alert">&times;</a> Favor de escribir o Seleccionar el Sistema correcto.</div>'
        $('#tags').focus();
        return false;
    }
    if ($('#sistema').val() == []) {
        $('#sistema').val(['']);
        //alert('Debe Seleccionar un Sistema');
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger"><a href="" class="close" data-dismiss="alert">&times;</a> Favor de escribir o Seleccionar el Sistema correcto.</div>'
        $('#sistema').focus();
        return false;
    }
    if ($('#fecha_solicitud').val() == []){
        $('fecha_solicitud').val(['']);
        //alert('Debe seleccionar el correcto')
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger"><a href="" class="close" data-dismiss="alert">&times;</a> Favor de Ingresar la fecha de Solicitud.</div>'
        $('#fecha_solicitud').focus();
        return false;
    }
    if ($('#descripcion').val() == []){
        $('descripcion').val(['']);
        //alert('Debe seleccionar el correcto')
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger"><a href="" class="close" data-dismiss="alert">&times;</a> Ingrese la descripción.</div>'
        $('#descripcion').focus();
        return false;
    }
    if ($('#solucion').val() == []){
        $('solucion').val(['']);
        //alert('Debe seleccionar el correcto')
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger"><a href="" class="close" data-dismiss="alert">&times;</a> Ingrese la solución.</div>'
        $('#solucion').focus();
        return false;

    }
    if ($('#estatus_solicitud').val() == []){
        $('estatus_solicitud').val(['']);
        //alert('Debe seleccionar el correcto')
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger"><a href="" class="close" data-dismiss="alert">&times;</a> Seleccione un estatus.</div>'
        $('#estatus_solicitud').focus();
        return false;

    }
    if ($('#tags_empleados').val() == []){
        $('tags_empleados').val(['']);
        //alert('Debe seleccionar el correcto')
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger"><a href="" class="close" data-dismiss="alert">&times;</a> Seleccione un solicitante.</div>'
        $('#tags_empleados').focus();
        return false;
    }
    $.ajax({
        url: "http://10.2.8.116/lista_actividades_genericas/",        
        method: 'post',
        data: $("#formularios").serialize(),
        success: function (data) {
            console.log(data) 
            $('#demo').DataTable().ajax.reload(); //Recargar el datatable para visualizar los datos actuales.
            CierraPopu();
        }, 
        error: function(error){
            console.log(error)
        }
    })
});

function CierraPopu(){
    $ ('#cerrar').click();//Esto simula un click sobre el botón close de la modal, por lo que no se debe preocupar por qué clases agregar o qué clases sacar.
    $ ('.modal-backdrop').remove();//eliminamos el backdrop del modal actividad
    }

$('#new').on('click', function() {
    const fecha = new Date();
    document.getElementById("fecha_solicitud").value = fecha.toJSON().slice(0,10);
    document.getElementById("alerta").innerHTML = "";
    $('#tags').val(['']);
    $('#sistema').val(['']);
    $('#solicitud').val(['']);
    $('#descripcion').val(['']);
    $('#solucion').val(['']);
    //$('#fecha_solicitud').val(['']);
    $('#fecha_solucion').val(['']);
    $('#tags_empleados').val(['']);
    $('#estatus_solicitud').val(['']);
    $('#estatus').val(['']);
    $('#solicita').val([''])
$('#type').val('new');
$('#modal_titlenew').text('NUEVA ACTIVIDAD');
$("#myModalnew").modal();

// cargar select y autocomplet.
});


$('#demo tbody').on('click', 'button', function () {
    let data = table.row($(this).parents('tr')).data();
    let class_name = $(this).attr('class');
    if (class_name == 'btn btn-info') {
        // EDIT button
        $('#tags').val(data['tags']);
        $('#sistema').val(data['sistema']);
        $('#solicitud').val(data['solicitud']);
        $('#descripcion').val(data['descripcion']);
        $('#solucion').val(data['solucion']);
        $('#fecha_solucion').val(data['fecha_solucion']);
        $('#tags_empleados').val(data['fecha_solucion']);
        $('#estatus_solicitud').val(data['fecha_solucion']);
        $('#estatus').val(data['estatus']);
        $('#solicita').val(data['solicita']);
        $('#tags_creador').val(data['tags_creador']);

        $('#type').val('edit');
        $('#modal_title').text('EDIT');
        $("#myModal").modal();
    } else {
        // DELETE button
        $('#modal_title').text('DELETE');
        $("#confirm").modal();
    }

    id = data['id'];

});

$('#confirm').on('click', '#delete', function (e) {
    $.ajax({
        url: 'http://10.2.8.116/delete-actividad/' + id + '/',
        method: 'DELETE'
    }).success(function (data, textStatus, jqXHR) {
        location.reload();
    }).error(function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR)
    });
});
