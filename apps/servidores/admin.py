from django.contrib import admin

from apps.servidores.models import estatus, puertos, servidor, sistema_operativo, t_dns, t_ip

# Register your models here.
admin.site.register(sistema_operativo)
admin.site.register(t_dns)
admin.site.register(t_ip)
admin.site.register(puertos)
admin.site.register(estatus)
admin.site.register(servidor)