from django.shortcuts import render
from apps import actividades
from apps.actividades.models import actividades
from apps.servidores.models import servidor
from apps.actividades.serializer import sistemas_serializars, sistemas_dns_serializars
from rest_framework.views import APIView, Response
from rest_framework import generics



# # Create your views here.
# class SistemasView(APIView):
    
#     serializer_class = sistemas_serializars

#     @staticmethod
#     def get(request):
#         name = request.GET.get('nota')
#         # usuarios = User.objects.filter(username__icontains=name is None)
#         # serializers = UsuarioSerializer(usuarios, many=True)

#         if name is not None:
#             sistema = servidor.objects.filter(nota__icontains=name)
#             serializers = sistemas_serializars(sistema, many=True)

#         else:
#             sistema = servidor.objects.all()ModelName

class SistemasView(APIView): #Este es el que cambie por el dns para que se visualizara en el autocomplete. SIN UTILIZAR
    
    serializer_class = sistemas_dns_serializars

    @staticmethod
    def get(request):
        name = request.GET.get('nota')
        # usuarios = User.objects.filter(username__icontains=name is None)
        # serializers = UsuarioSerializer(usuarios, many=True)

        if name is not None:
            sistema = servidor.objects.filter(nota__icontains=name)
            serializers = sistemas_serializars(sistema, many=True)

        else:
            sistema = servidor.objects.all()
            serializers = sistemas_serializars(sistema, many=True)
        return Response(serializers.data)