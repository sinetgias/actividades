from django.db import models

# Create your models here.
class sistema_operativo(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length = 300, blank= True, null=True, default='')

    def __str__(self):
        return '{}'.format(self.nombre)

class t_dns(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length = 300, null= True, blank= True, default='')
    
    def __str__(self):
        return '{}'.format(self.nombre)

class t_ip(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=200, blank=True, null=True, default='')

    def __str__(self):
        return '{}'.format(self.nombre)

class puertos(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=300, blank=True, null=True, default='')

    def __str__(self):
        return '{}'.format(self.nombre)

class estatus(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=300, blank=True, null=True, default='')

    def __str__(self):
        return '{}'.format(self.nombre)

class servidor(models.Model):
    ip = models.ForeignKey(t_ip, null=True, blank=True, on_delete=models.CASCADE)
    dns = models.ForeignKey(t_dns, null = True, blank = True, on_delete=models.CASCADE)
    puerto = models.ForeignKey(puertos, null=True, blank=True, on_delete=models.CASCADE)
    ambiente = models.CharField(max_length=100)
    os = models.ForeignKey(sistema_operativo, null = True, blank = True, on_delete=models.CASCADE)
    estatus = models.ForeignKey(estatus, blank=True,null=True, on_delete= models.CASCADE)
    fecha_creacion = models.DateField(null= True, blank= True)
    fecha_modificacion = models.DateField(null=True, blank=True)
    nota = models.CharField(max_length=300)

    def __str__(self):
        return '{}'.format(self.dns)