from multiprocessing.sharedctypes import Value
from tkinter.tix import Form
from django import forms
from django.forms import IntegerField, widgets
from apps.actividades.models import actividades

class AddForm(forms.ModelForm):

    # def clean(self, *args, **kwargs):
    #     cleaned_data = super(AddForm,self).clean(*args, **kwargs)
    #     descripcion = cleaned_data.get('descripcion', None)
    #     print("Esto es la :", descripcion)
    #     if descripcion is not None:
    #         if descripcion == None:
    #             self.add_error('descripcion', '"0" No es un periodo permitido')

    sistemashidden = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control empty','minlength': '2', 'id': 'sistemashidden', 'placeholder': 'Ingrese el Sistema', 'Value':0})) # Este es el imput oculto donde se agrega el id del autoselect

    solicitantehidden = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control empty','minlength': '2', 'id': 'solicitantehidden', 'placeholder': 'Ingrese el Solicitante', 'Value':0})) # Este es el imput oculto donde se agrega el id del autoselect

    sistemas = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control round','minlength': '2', 'id': 'tags', 'placeholder': 'Ingrese el Sistema'}))

    fecha_solicitud = forms.DateField(label="Fecha Solicitud:", input_formats=['%Y-%m-%d'],
                                       widget=forms.TextInput(attrs={'class':'btn-sm form-control round dateinput icon wb-calendar','span':'input-group-prepend', 'type':'date'}))
    
    #estatus = forms.CharField(widget=forms.Select(attrs={'class':'form-control'}))

    solicitas = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control round','minlength': '2', 'id': 'tags_empleados', 'placeholder': 'Quien Solicita'}))                                       


    class Meta:
        
        model = actividades
        Fields = [
            'id',
            'sistemas',
            'sistemashidden',
            'solicitantehidden',
            'fecha_solicitud',
            'descripcion',
            'solucion',
            'fecha_solucion',
            'estatus',
            'solicita',
            'crea_solicitud'
        ]
        exclude = [
            'sistema',
            'solicita',
    
        ]
        labels = {
            # 'sistemas': 'Sistemas',
            #'fecha_solicitud': 'Fecha Solicitud',
            #'descripcion': 'Descripciòn',
            #'solucion': 'Soluciòn',
            #'fecha_solucion': 'Fecha de Soluciòn',  
            #'estatus': 'Estado',
            #'solicita': 'Solicita',
        }
        widgets = {
        #     #'sistema': forms.ComboField(attrs={'class':'form-control'}),
        #     # 'fecha_solicitud': forms.DateInput(format=('%Y-%m-%d'),
        #     #     attrs={'class':'btn-sm form-control dateinput icon wb-calendar','span':'input-group-prepend', 'type':'date'}),
            'descripcion':forms.TextInput(attrs={'class':'form-control round'}),
            'solucion':forms.TextInput(attrs={'class':'form-control round'}),
            'fecha_solucion':forms.DateInput(format=('%Y-%m-%d'),attrs={'class':'form-control round', 'type':'date'}),
            'estatus':forms.Select(attrs={'class':'form-control round'}),
            #'solicita':forms.NumberInput(attrs={'class':'form-control round'}),   
        }
class UpdateActividadForms(forms.ModelForm):

   
    fecha_solicitud = forms.DateField(label="Fecha Solicitud:", input_formats=['%Y-%m-%d'],
                                    widget=forms.TextInput(attrs={'class':'btn-sm form-control round dateinput icon wb-calendar','span':'input-group-prepend', 'type':'date'}))


    tags_creador = forms.CharField(widget=forms.TextInput(
attrs={'class': 'form-control round','minlength': '2', 'id': 'tags_creador', 'placeholder': 'Quien Solicita'}))
    
    class Meta:
        model = actividades
        fields = [


            'fecha_solicitud',
            'descripcion',
            'solucion',
            'fecha_solucion',
            'estatus',
            'tags_creador'
        ];
        Exclude = [
            'sistema',
            'solicita',
            'crea_solicitud'
        ]
        labels = {
            # 'sistemas': 'Sistemas',
            #'fecha_solicitud': 'Fecha Solicitud',
            #'descripcion': 'Descripciòn',
            #'solucion': 'Soluciòn',
            #'fecha_solucion': 'Fecha de Soluciòn',  
            #'estatus': 'Estado',
            #'solicita': 'Solicita',
        }
        widgets = {
        #     #'sistema': forms.ComboField(attrs={'class':'form-control'}),
        #     # 'fecha_solicitud': forms.DateInput(format=('%Y-%m-%d'),
        #     #     attrs={'class':'btn-sm form-control dateinput icon wb-calendar','span':'input-group-prepend', 'type':'date'}),
            'descripcion':forms.TextInput(attrs={'class':'form-control round'}),
            'solucion':forms.TextInput(attrs={'class':'form-control round'}),
            'fecha_solucion':forms.DateInput(format=('%Y-%m-%d'),attrs={'class':'form-control round', 'type':'date'}),
            'estatus':forms.Select(attrs={'class':'form-control round'}),
            #'solicita':forms.NumberInput(attrs={'class':'form-control round'}),   
        }