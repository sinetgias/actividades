from ast import Delete
from audioop import add
from re import template
from typing import Generic
from urllib import request, response
import django
from django import views
from django.db import IntegrityError
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render
from django.views import View
from django.views.generic.edit import FormView

from apps.actividades import serializer
from apps.actividades.forms import AddForm, UpdateActividadForms
from apps.actividades.models import actividades, estatus
from apps.servidores.models import t_dns
from apps.usuarios.models import User
from django.urls import reverse, reverse_lazy
# Create your views here.
from django.views.generic import CreateView, TemplateView, DeleteView, UpdateView
from rest_framework.views import APIView, Response
from django_datatables_view.base_datatable_view import BaseDatatableView
from rest_framework import generics
from apps.usuarios.views import login
from django.db import transaction
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db.models import Q

@method_decorator(login_required, name='dispatch') #Decorador usado para el TemplateView para loguearse
class ListaActividad(TemplateView):
    template_name= 'base/base.html'

    def get_context_data(self,**kwargs):
        context = super(ListaActividad,self).get_context_data(**kwargs)
        act= actividades.objects.all
        context ['dirigidoc']= act
        return context

class Create_actividad(LoginRequiredMixin ,FormView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    model = actividades
    form_class = AddForm
    template_name = 'alta_Actividades.html'
    success_url = '/listar/'
    

    def form_valid(self,form):
        try:
            sistemas = int(self.request.POST.get('sistemashidden')) # Se Agrega en la variable sistemas el valor int del Input oculto, valor agregado desde el AOUTOCOMPLET
            int(sistemas)
            #print(type(sistemas))
            solicita = int(self.request.POST.get('solicitantehidden')) # Se Agrega en la variable solicita el valor del Input oculto, valor agregado desde el AOUTOCOMPLET
            int(solicita)
            #print(type(solicita))
            crea_actividad = int(self.request.POST.get('tags_creador'))

            if sistemas == 0:
                messages.add_message(self.request, messages.WARNING,'Seleccione el Sistema Correcto')
                return HttpResponseRedirect(reverse('add'))
            
            if solicita == 0:
                messages.add_message(self.request, messages.WARNING,'Seleccione al Solicitante correcto')
                return HttpResponseRedirect(reverse('add'))
            if sistemas == 0 and solicita == 0:
                messages.add_message(self.request, messages.WARNING,'Verifique sus selecciones')
                return HttpResponseRedirect(reverse('add'))
            else:
                form.instance.sistema_id=sistemas    # Pasamos el valor de la variable a la instancia de BDatos tal cual se llame el campo de la DB.
                form.instance.solicita_id= solicita # Pasamos el valor de la variable a la instancia de BDatos tal cual se llame el campo de la DB.
                form.instance.crea_solicitud_id = crea_actividad
                form.save()
            messages.add_message(self.request, messages.WARNING,'Registro Exitoso')

        except IntegrityError:
            messages.add_message(self.request,messages.WARNING, 'Error de Integridad')
        except TypeError:
            messages.add_message(self.request, messages.WARNING, 'Verificar la Falla del Error')
        return HttpResponseRedirect(reverse('actividades_index'))

class lista_empleado(LoginRequiredMixin, APIView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    serializer_class = serializer.solicita_serializar
    @staticmethod
    def get(request):
        solicita = request.GET.get('first_name')
        if solicita is not None :
            list_solicita = User.objects.filter(first_name__icontains = solicita)
            serializers = serializer.solicita_serializar(list_solicita, many=True)
        else:
            list_solicita = User.objects.all()
            serializers = serializer.solicita_serializar(list_solicita, many=True)

        return Response(serializers.data)

class lista_sistemas(LoginRequiredMixin, APIView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    serializer_class = serializer.sistemas_dns_serializars
    @staticmethod
    def get(request):
        sistema = request.GET.get('nombre')
        if sistema is not None :
            list_sistema = t_dns.objects.filter(nombre__icontains = sistema)
            serializers = serializer.sistemas_dns_serializars(list_sistema, many=True)
        else:
            list_sistema = t_dns.objects.all()
            serializers = serializer.sistemas_dns_serializars(list_sistema, many=True)

        return Response(serializers.data)
class lista_actividad_API(LoginRequiredMixin, APIView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    serializer_class = serializer.lista_actividades_serializars
    @staticmethod
    def get(request):
        actividad = request.GET.get('pk, 1')
        print(actividad)
        if actividad is not None:
            lista_activ = actividades.objects.filter(descripcion__icontains = actividad)
            serializers = serializer.lista_actividades_serializars(lista_activ, many=True)
            print(serializers)

        else: 
            lista_activ = actividades.objects.all()
            serializers = serializer.lista_actividades_serializars(lista_activ, many=True)
        return Response(serializers.data)

class Actividades_list_json(BaseDatatableView):
    model = actividades
    colums = [
        'pk',
        'sistema',
        'fecha_solicitud',
        'descripcion',
        'solucion',
        'fecha_solucion',
        'estatus',
        'solicita',
        ''
    ]
class actividades_generalesView(LoginRequiredMixin, TemplateView): #OPCION 2 PARA EL login USANDO LoginRequiredMixin(se tiene que llamar la libreria) y usando las dos lineas de abajo. 
    login_url = '/login'
    redirect_field_template = 'inicio'
    template_name = 'actividades/lista_actividades.html'

class datatable_actividadesView(BaseDatatableView): # Las comillas vacias es porque se agrega la columna de ACCIONES, debe corresponder/
                                                    # con la columna del HTML de lista_actividades.html

    model = actividades
    columns = [
        'pk',
        'sistema',
        'fecha_solicitud',
        'descripcion',
        'solucion',
        'fecha_solucion',
        'estatus',
        'solicita',
        'crea_solicitud'
    ]
    def get_initial_queryset(self):
        # return queryset used as base for further sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return actividades.objects.filter(crea_solicitud= self.request.user)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        print(search)
        if search:
            qs = qs.filter(Q(sistema_id__nombre__icontains=search)|Q(solicita_id__username__icontains=search) )
                
        return qs


class Actividades_List_API(LoginRequiredMixin, generics.ListCreateAPIView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    queryset= actividades.objects.all()
    serializer_class = serializer.lista_actividades_serializars

class Update_actividad(LoginRequiredMixin, UpdateView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    model = actividades
    form_class = UpdateActividadForms
    template_name = 'actualiza_actividad.html'
    
    def get_success_url(self):
        print(self.object);
        # messages.success(self.request,'El prestamo de  ' + self.object.empleado.nombre + ' se edito exitosamente' + 'con ClAVE ' + str(self.object.id))  # el object sabe de la relaxion
        return reverse_lazy('inicio')
        #get initia
    
    @transaction.atomic()
    def form_valid(self, form):
        crea_actividad = int(self.request.POST.get('solicita'))
        sistemas = int(self.request.POST.get('sistema'))
        print("Estos es el numero del Sistema:  ",sistemas)
        print("Este es quien solicita: ",crea_actividad)
        print(self.object);
        obj_form = form.save(commit=False)  # obj.form.save para modificar el form  valid original y agregarle algun otro objecto
        form.instance.solicita_id = crea_actividad
        form.instance.sistema_id=sistemas    # Pasamos el valor de la variable a la instancia de BDatos tal cual se llame el campo de la DB.
        obj_form.save()
        return HttpResponseRedirect(reverse('actividades_generales'))
class Eliminar_Actividad(LoginRequiredMixin, DeleteView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    template_name = "actividad_delete.html"
    model = actividades
    success_url = reverse_lazy('actividades_generales')

    def get_success_url(self):
        return reverse('actividades_generales')