import re
from typing import no_type_check
from django.db import models
from django.urls import reverse
from apps.servidores.models import servidor, t_dns
from apps.usuarios.models import User
# Create your models here.
from django.db import models

class estatus(models.Model):
    nombre = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=150)

    def __str__(self):
        return '{}'.format(self.nombre)

class actividades(models.Model):
    sistema = models.ForeignKey(t_dns, blank=True, null=True, on_delete=models.CASCADE)
    fecha_solicitud = models.DateField(blank=True, null=True)
    descripcion = models.CharField(max_length = 300)
    solucion = models.CharField(max_length= 500, null=True, blank=True)
    fecha_solucion = models.DateField(blank=True, null=True)
    estatus = models.ForeignKey(estatus, null=True, blank=True,default=1, on_delete=models.CASCADE)
    solicita = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name = 'solicita')
    crea_solicitud = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name='crea_solicitud')

    def __str__(self):
        return '{}'.format(self.descripcion)