from django.contrib import admin
from django.contrib.admin.decorators import action

from apps.actividades.models import estatus, actividades

# Register your models here.
admin.site.register(estatus)
admin.site.register(actividades)