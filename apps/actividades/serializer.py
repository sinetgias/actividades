from dataclasses import field
from django.shortcuts import render
from rest_framework import serializers
from apps.actividades.models import actividades

from apps.servidores.models import servidor, t_dns
from apps.usuarios.models import User

# Create your views here.
class sistemas_serializars(serializers.ModelSerializer):
    class Meta:
        model = servidor
        fields = ['id',
        'ambiente',
        'fecha_creacion',
        'fecha_modificacion',
        'nota',
        'ip',
        'dns',
        'puerto',
        'os',
        'estatus']
        
class sistemas_dns_serializars(serializers.ModelSerializer):
    class Meta:
        model = t_dns
        fields = [
            'id',
            'nombre',
            'descripcion',
        ]



class solicita_serializar(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'first_name',
            'last_name',
            'telefono',
            'ciudad',
            'user_dependencia',
            'descripcion'
        ]

class lista_actividades_serializars(serializers.ModelSerializer):
    class Meta:
        model = actividades
        fields = [
            'id',
            'sistema',
            'fecha_solicitud',
            'descripcion',
            'solucion',
            'fecha_solucion',
            'estatus',
            'solicita',
            'crea_solicitud'
        ]
