from contextvars import Context
import email
from multiprocessing import context
from pickletools import read_uint1
from django import views
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate,login as do_login
from django.contrib.auth import logout as do_logout
from django.urls import reverse
from django.views.generic import TemplateView
from apps.actividades.models import estatus,actividades
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator    #   PASO1 PARA USAR EL @method_decorator 
from django.contrib.auth.decorators import login_required # PASO1 ESTE SE LLAMA PARA USARLO DENTRO DE @method_decorator(login_required) 
                                                        #USAOD EN settings.py LOGIN_URL= '/login' CON LAS FUNCIONES DE MAS EN  class index(TemplateView):

# Create your views here.

class index(TemplateView):
    login_url = '/login'
    redirect_field_name = 'inicio'
    template_name = "actividades/lista_actividades.html"
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def estatus(self):
        return estatus.objects.all()

# def bienvenido(request):
#     if request.user.is_authenticated:
#         return render (request,'actividades/lista_actividades.html')
#     return redirect('../login')

def login(request):
    
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            print(username)
            password = form.cleaned_data['password']
            user = authenticate(username = username, password = password)
            if user is not None:
                do_login(request, user)
                return redirect('/')
    return render(request, "usuarios/login.html", {'form': form})

def logout(request):
    do_logout(request)
    return redirect('/login')