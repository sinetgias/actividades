from django.contrib import admin
from django.contrib.auth.admin import UserAdmin,Group
from django.db import models
from apps.usuarios.models import User
from apps.servidores.models import sistema_operativo,t_dns, t_ip, puertos, estatus, servidor
from apps.dependencias.models import dependencia

# Register your models here.
class UserAdmin(UserAdmin):
    
    model = User
    prepopulated_fields = {
        'username': ('first_name', 'last_name',)}  # AUTORELLENAR EL CAMPO USERNAME CON LOS DOS ANTERIOES#

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'username', 'password1', 'password2', 'email', 'telefono', 'ciudad',
                       'user_dependencia', 'descripcion'
                       )
        }
         ),
    )
    ordering = ('first_name',)

    fieldsets = UserAdmin.fieldsets + (  # PARA AGREGAR AL ADMIN DJANGO UN CAMPO NUEVO AL MODIFICAR#
        ('Personal Info Extra', {'fields': ('telefono', 'ciudad', 'user_dependencia', 'descripcion',)}),
    )
    list_display = ['username', 'email', 'telefono', 'ciudad', 'user_dependencia', 'descripcion', ]

admin.site.register(User, UserAdmin)
