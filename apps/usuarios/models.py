from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.base import Model
from django.db.models.fields import BLANK_CHOICE_DASH, IPAddressField
from apps.dependencias.models import dependencia


from apps import dependencias
# Create your models here.

class User(AbstractUser):
    telefono = models.CharField(max_length=20, blank=True, default='', null=True)
    ciudad = models.CharField(max_length=70, blank=True, default='', null=True)
    user_dependencia = models.ForeignKey(dependencia, null=True, blank=True, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=300, blank=True, default='', null=True)