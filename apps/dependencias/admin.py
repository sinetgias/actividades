from django.contrib import admin
from apps import dependencias

from apps.dependencias.models import departamento, unidad_administrativa, dependencia

# Register your models here.
admin.site.register(unidad_administrativa)
admin.site.register(departamento)
admin.site.register(dependencia)