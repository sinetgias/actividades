from django.db import models

# Create your models here.
class unidad_administrativa(models.Model):
    nombre_corto = models.CharField(max_length=30)
    descripcion = models.CharField(max_length= 300, blank=True, null=True, default= '')

    def __str__(self):
        return '{}'.format(self.nombre_corto)

class departamento(models.Model):
    nombre_corto = models.CharField(max_length=30)
    unid_administrativa = models.ForeignKey(unidad_administrativa,blank=True, null=True, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length= 300, null=True, blank=True, default= '')

    def __str__(self):
        return '{}'.format(self.nombre_corto)

class dependencia(models.Model):
    nombre_corto = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=300, blank = True,  null=True, default = '')
    dep_Departamento = models.ForeignKey(departamento, null= True, blank= True, on_delete=models.CASCADE)
    
    def __str__(self):
        return '{}'.format(self.nombre_corto)